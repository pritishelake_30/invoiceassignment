package com.application.main.dto;

//@Data
public class CustomerDto {

	private Integer id;
	private String invoiceNo;
	private String billTo;
	private String invoiceDate;
	private String dueDate;
	private String status;
	private Long amount;
	private String product;
	private String description;
	private Long price;
	private Integer qty;
	private Long total;
	private String invoiceNote;

	public Integer getid() {
		return id;
	}

	public void setid(Integer id) {
		this.id = id;
	}

	public String getinvoiceNo() {
		return invoiceNo;
	}

	public void setinvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getbillTo() {
		return billTo;
	}

	public void setbillTo(String billTo) {
		this.billTo = billTo;
	}

	public String getinvoiceDate() {
		return invoiceDate;
	}

	public void setinvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setdueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getstatus() {
		return status;
	}

	public void setstatus(String status) {
		this.status = status;
	}

	public Long getamount() {
		return amount;
	}

	public void setamount(Long amount) {
		this.amount = amount;
	}

	public String getproduct() {
		return product;
	}

	public void setproduct(String product) {
		this.product = product;
	}

	public String getdescription() {
		return description;
	}

	public void setdescription(String description) {
		this.description = description;
	}

	public Long getprice() {
		return price;
	}

	public void setprice(Long price) {
		this.price = price;
	}

	public Integer getqty() {
		return qty;
	}

	public void setqty(Integer qty) {
		this.qty = qty;
	}

	public Long gettotal() {
		return total;
	}

	public void settotal(Long total) {
		this.total = total;
	}

	public String getinvoiceNote() {
		return invoiceNote;
	}

	public void setinvoiceNote(String invoiceNote) {
		this.invoiceNote = invoiceNote;
	}

	
	
	
	

	
	
	
}