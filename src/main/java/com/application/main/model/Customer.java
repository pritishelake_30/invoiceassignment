package com.application.main.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import lombok.Data;
@Entity
@Table(name="customerdata")
@Data
public class Customer {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	@Column(name="id")
	private Integer id;

	
	
	@Column(name="invoiceno")
	private String invoiceNo;
	
	
	
	@Column(name="billto")
	private String billTo;
	
	@Column(name="invoicedate")
	private String invoiceDate;
	
	@Column(name="duedate")
	private String dueDate;
	
	@Column(name="status")
	private String status;
	
	@Column(name="amount")
	private Double amount;
	
	@Column(name="product")
	private String product;
	
	@Column(name="description")
	private String description;
	
	@Column(name="price")
	private Long price;
	
	@Column(name="qty")
	private Integer qty;
	
	@Column(name="total")
	private Long total;
	
	@Column(name="tax")
	private Double tax;
	
	@Column(name="invoicenote")
	private String invoiceNote;

	public Integer getid() {
		return id;
	}

	public void setid(Integer id) {
		this.id = id;
	}

	public String getinvoiceNo() {
		return invoiceNo;
	}

	public void setinvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	
	public String getbillTo() {
		return billTo;
	}

	public void setbillTo(String billTo) {
		this.billTo = billTo;
	}
	 
	public String getinvoiceDate() {
		return invoiceDate;
	}

	public void setinvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getdueDate() {
		return dueDate;
	}

	
	public void setdueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getstatus() {
		return status;
	}

	public void setstatus(String status) {
		this.status = status;
	}

	public Double getamount() {
		return amount;
	}

	public void setamount(Double amount) {
		this.amount = amount;
	}

	public String getproduct() {
		return product;
	}

	public void setproduct(String product) {
		this.product = product;
	}

	public String getdescription() {
		return description;
	}

	public void setdescription(String description) {
		this.description = description;
	}

	public Long getprice() {
		return price;
	}

	public void setprice(Long price) {
		this.price = price;
	}

	public Integer getqty() {
		return qty;
	}

	public void setqty(Integer qty) {
		this.qty = qty;
	}

	public Long gettotal() {
		return total;
	}

	public void settotal(Long total) {
		this.total = total;
	}

	public Double gettax() {
		return tax;
	}

	public void settax(Double tax) {
		this.tax = tax;
	}

	public String getinvoiceNote() {
		return invoiceNote;
	}

	public void setinvoiceNote(String invoiceNote) {
		this.invoiceNote = invoiceNote;
	}

	

	

	

}
