package com.application.main.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.application.main.model.Customer;

@Repository
public interface CustomerRepo extends JpaRepository<Customer, Long> {
//void deleteById(Integer id);

Customer findById(Integer id);

void deleteById(int id);
}

