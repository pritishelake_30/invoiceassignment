package com.application.main.serviceImpl;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.application.main.dto.CustomerDto;
import com.application.main.model.Customer;
import com.application.main.repository.CustomerRepo;
import com.application.main.service.CustomerService;


@Service
public class CustServiceImpl implements CustomerService{

	@Autowired
	private CustomerRepo customerrepo;

	@Autowired
	ModelMapper mapper;

	@Override
	public void createOrUpdateCustomer(CustomerDto custDto) {
		Customer customer = mapper.map(custDto, Customer.class);
	customerrepo.save(customer);
	}
	
	public CustomerDto editCustomer(Integer id) {
	Customer cust = customerrepo.findById(id);
	return mapper.map(cust, CustomerDto.class);
	}

	

	

	@Override
	public void deleteCustomer(int id) {
	System.out.println("given id is:"+id);

	customerrepo.deleteById(id);
	System.out.println("deleted customer successfully");

	}

	@Override
	public List<CustomerDto> getAllCustomers() {
		List<Customer> customer = customerrepo.findAll();
		List<CustomerDto> customerDto = mapper.map(customer, new TypeToken<List<CustomerDto>>() {
		}.getType());

		return customerDto;
		}

	

}
	