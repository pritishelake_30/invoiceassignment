package com.application.main.controller;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.application.main.dto.CustomerDto;
import com.application.main.service.CustomerService;




@Controller
public class InvoiceController {

	@Autowired
	private CustomerService customerservice;
	
	@GetMapping("/registration")
	public String reg(Map<String, Object> model) {
		model.put("Customer", new CustomerDto());
		return "Registration";
	}
	
	@PostMapping("/home")
	public String createCustomer(@ModelAttribute("customer") CustomerDto custDto) {
		customerservice.createOrUpdateCustomer(custDto);
		return "redirect:/list";	
	}
	
	
	  @GetMapping("/list") public String listOfCustomer(Model model) {
	  List<CustomerDto> customerList=customerservice.getAllCustomers();
	  model.addAttribute("custList", customerList);
	  return "CustomerList"; }
	 
	
	
	  @DeleteMapping("/delete")
	  @Transactional
	  public String deleteCustomer(@RequestParam("id")int id)
	  { 
		  System.out.println("Given id is :"+id);
		  customerservice.deleteCustomer(id);
		  return "redirect:/list"; }
	  
	  @GetMapping("/edit")
	  public String editCustomer(@RequestParam("id") Integer id, Map<String, Object> model) {
	  CustomerDto custDto = customerservice.editCustomer(id);
	  model.put("Customer", custDto);
	  return "Registration";
	  }
	 
	
}
