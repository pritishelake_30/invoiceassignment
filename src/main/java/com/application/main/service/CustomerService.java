package com.application.main.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.application.main.dto.CustomerDto;


@Service
public interface CustomerService {

	public List<CustomerDto> getAllCustomers();
	
    void createOrUpdateCustomer(CustomerDto custDto);
	
	public void deleteCustomer(int id);

	public CustomerDto editCustomer(Integer id);
}
