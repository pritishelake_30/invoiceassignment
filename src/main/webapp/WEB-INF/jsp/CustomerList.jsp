<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="stag"%>
<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" type="text/css"
	href="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css" />
</head>
<body style="background-color: #FFFFE0;">
	<h2>Customer List</h2>

<table id="data" border="1px solid black">
			<thead>
		<tr>
			
			<th>BillTo</th>
			<th>InvoiceDate</th>
			<th>DueDate</th>
			<th>Status</th>
			<th>amount</th>
			<th>Action</th>
			<th></th>
		</tr>
		<thead>
		<c:forEach items="${custList}" var="cust">
			<tbody>

				<tr>
		
						<td>${cust.billTo}</td>
						<td>${cust.invoiceDate}</td>
						<td>${cust.dueDate}</td>
						<td>${cust.status}</td>
						<td>${cust.amount}</td>

				<td>
						<button><a href="/edit?id=${cust.id}">Edit</a></button>
						</td>
				<td>
					<form action="/delete?id=${cust.id}" method="post">
						<input type="submit" value="Delete"
							style="background: none; border: 0px; cursor: pointer;" />
					</form>
				</td>
			</tr>
		</tbody>	
		</c:forEach>
	</table>
	<script
		type="text/javascript"
		charset="utf8"
		src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"
></script>
<script
		type="text/javascript"
		charset="utf8"
		src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
<script>
	$(function() {
		$("#data").dataTable();
	});
</script>
<script>
$(document).ready(function () {
  $('#dtBasicExample').DataTable({
    "pagingType": "simple-numbers"
  });
  $('.dataTables_length').addClass('bs-select');
});
</script>

</body>
</html>