<%@ page import="java.sql.Statement"%> 
 <%@ page import="java.sql.Connection"%> 
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 <%@ page import="java.sql.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%> 
<html>
<head>
<meta charset="ISO-8859-1">
<title>Invoice Data</title>

<link rel="stylesheet" 
		type="text/css"
			href="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css" />
		
<style>
		td, th {
			font-family: arial, sans-serif;
		}
		</style>
<script
		type="text/javascript"
		charset="utf8"
		src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"
></script>
<script
		type="text/javascript"
		charset="utf8"
		src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
<script>
	$(function() {
		$("#data").dataTable();
	});
</script>

</head>

<body style="background-color: #FFFFE0;">
	<div style="margin-top: 50px; margin-left: 250px; height: 50px;">
		<h2>
			Customer
			<c:out value="${customer.id != null ? 'Update' : 'Info' }" />
		</h2>
	</div>

	<form:form method="POST" modelAttribute="Customer" action="/home"
		name="customer">

		<table style="vertical-align: center; margin-left: 20%;">

			<tr>
				<td><form:hidden path="id" /></td>
			</tr>


			<tr>
				<!-- This is Bill To: Wala tr -->
				<td>BillTo:</td>
                <td><%!String driverName = "com.mysql.jdbc.Driver";%> <%!String url = "jdbc:mysql://localhost:3306/invoicedata";%>
					<%!String user = "root";%> <%!String psw = "root";%>
					<form action="#">
						<%
						Connection con = null;
						PreparedStatement ps = null;
						try {
							Class.forName(driverName);
							con = DriverManager.getConnection(url, user, psw);
							String sql = "SELECT * FROM customer";
							ps = con.prepareStatement(sql);
							ResultSet rs = ps.executeQuery();
						%>

						<form:select path="billTo">
							<option value="none" selected disabled hidden>Select an
								Option</option>
							<%
							while (rs.next()) {
								String fname = rs.getString("customer_name");
							%>
							<option value="<%=fname%>"><%=fname%></option>
							<%
							}
							%>
						</form:select>
						<%
						} catch (SQLException sqe) {
						out.println(sqe);
						}
						%>
					</td> 
				<td>Invoice Date :</td>
				<td><form:input path="invoiceDate" type="date" id="invoice" /></td>
			</tr>

			<tr>
				<td>Invoice no :</td>
				<td><form:input path="invoiceNo" id="userName" /></td>

				<td>Due date :</td>
				<td><form:input path="dueDate" type="date" id="due" /></td>
			</tr>

			<tr>
				<td>Status :</td>
				<td><form:select path="status" name="sel">
						<option value="none" selected disabled hidden>Select an
							Option</option>
						<option value="Draft">Draft</option>
						<option value="Sent">Sent</option>
						<option value="Paid">Paid</option>
					</form:select></td>
			</tr>

			<tr>
				<td colspan="2"><input type="submit"
					value="<c:out value="${customer.id != null ? 'Update' : 'Register' }" />"
					onclick="return validate();">&nbsp;&nbsp; 
				</td>
			</tr>

		</table>
</form:form>
		<table id="example" class="display" style="width: 100%" border="1px solid black">
			<thead>
				<tr>
					<th>Id</th>
					<th>Product</th>
					<th>Description</th>
					<th>Price</th>
					<th>Qty</th>
					<th>Tax(%)</th>
					<th>Total</th>
					<th></th>
				</tr>
			</thead>

			<tbody>
				<tr>
					<td>#</td>


					<td><select size="1" id="row-57-office" name="row-57-office">
							<option value="none" selected disabled hidden>Select a
								product</option>
							<option value="Monitor">Monitor</option>
							<option value="Mouse">Mouse</option>
							<option value="CPU">CPU</option>
					</select></td>
					<td><input type="text" id="row-57-position"
						name="row-57-position"></td>
					<td><input type="number" id="price" name="row-57-position"></td>
					<td><input type="number" id="qty" name="row-57-position"></td>
					<td><input type="number" id="tax" name="row-57-position"></td>
					<td></td>
					<td>
						<form action="/delete?id=${cust.id}" method="post">
							<input type="submit" value="Delete" />
						</form>
					</td>
				</tr>

				<tr>
					<td>#</td>
					<td><select size="1" id="row-57-office" name="row-57-office">
							<option value="none" selected disabled hidden>Select a
								product</option>
							<option value="Monitor">Monitor</option>
							<option value="Mouse">Mouse</option>
							<option value="CPU">CPU</option>
					</select></td>
					<td><input type="text" id="row-57-position"
						name="row-57-position"></td>
					<td><input type="number" id="price" name="row-57-position"></td>
					<td><input type="number" id="qty" name="row-57-position"></td>
					<td><input type="number" id="tax" name="row-57-position"></td>
					<td></td>
					<td>
						<form action="/delete?id=${cust.id}" method="post">
							<input type="submit" value="Delete" />
						</form>
					</td>
				</tr>

			</tbody>
		</table>
		
		
	<button id="addRow" onclick="addRow('example')">Add new</button>
	

	<script>$(document).ready(function() {
		    var table = $('#example').DataTable({
		        columnDefs: [{
		            orderable: false,
		            targets: [1,2,3]
		        }]
		    });
		
		    
		} );</script>


				<SCRIPT language="javascript">  
             	function addRow(tableID) {

			var table = document.getElementById(tableID);

			var rowCount = table.rows.length;
			var row = table.insertRow(rowCount);

			var colCount = table.rows[0].cells.length;

			for(var i=0; i<colCount; i++) {
var abc = 2;
				var newcell	= row.insertCell(i);

				newcell.innerHTML = table.rows[1].cells[i].innerHTML;
				//alert(newcell.childNodes);
				switch(newcell.childNodes[0].type) {
					case "text":
							newcell.childNodes[0].value = "";
							break;				   
					case "checkbox":
							newcell.childNodes[0].checked = false;
							break;
					case "select-one":
							newcell.childNodes[0].selectedIndex = 0;
							break;
				}
			}
		}
              	
             	
             	</script>

		<script>
		$('#due').on('change', function(){
		var startDate = $('#invoice').val();
		var endDate = $('#due').val();
		if (endDate < startDate){
		alert('DueDate should be greater than Start date.');
		$('#due').val('');
		}
		});
		</script>
	<form action="/action_page.php">
  Invoice Note:<input type="text" ><br><br>
  Attachment: <input type="file" id="myFile" name="filename">
  <br><br>
<button class="btn btn-warning btn-sm float-center">Save</button>
  <button class="btn btn-warning btn-sm float-center">Cancel</button>
		
</body>

</html>